# Lab set-up

## Requirements

* Docker CE

## Instructions

### CentOS Stream

> Tested on a fresh VM running [CentOS Stream 8](https://centos.org/centos-stream/).
> Skip to step 2 if you already have a working Docker set-up

```bash
# 1) Set up Docker CE
$ sudo yum install yum-utils.noarch
$ sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
$ sudo yum install docker-ce docker-ce-cli containerd.io
$ sudo usermod -aG docker $(whoami)
$ newgrp docker
$ sudo systemctl enable --now docker.service
```

### Ubuntu 20.04

> Tested on a fresh VM running [Ubuntu Server 20.04.2](https://ubuntu.com/download/server).
> Skip to step 2 if you already have a working Docker set-up

```bash
# 1) Set up Docker CE
$ sudo addgroup --system docker
$ sudo usermod -aG docker $(whoami)
$ newgrp docker
$ sudo snap install docker
```

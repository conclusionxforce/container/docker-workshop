#!/bin/bash


allImages=(nginx node:16-alpine mcr.microsoft.com/dotnet/aspnet:2.1 mcr.microsoft.com/dotnet/sdk:2.1 php:7.3-apache php:7.3-alpine golang:1.11.2 mongo traefik:alpine postgres redis:alpine python:3.8-slim-buster)

for images in ${allImages[@]}; do
  docker pull $images
done